package com.coderiders.learnlibgdx.threeD;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

public class ThreeD implements ApplicationListener {

	private AssetManager assetManager;
	private PerspectiveCamera cam;
	private CameraInputController camController;
	private ModelInstance player;
	private boolean playerNotInited = true;
	private ModelBatch modelBatch;
	private Environment environment;
	
	private ModelBuilder md;
	private Model cm;
	private ModelInstance ci;
	

	//Animation
	private AnimationController animationController;
	
	// Performance
	private Long createTime, playerLoadTime, playerLoadDuration;

	@Override
	public void create() {
		md = new ModelBuilder();
        cm= md.createBox(5f, 5f, 5f,
            new Material(ColorAttribute.createDiffuse(Color.GREEN)),
            Usage.Position | Usage.Normal);
        ci = new ModelInstance(cm);
        
        
		createTime = TimeUtils.millis();
		Gdx.app.log(this.getClass().getSimpleName(), "Create() called at: "
				+ createTime);
		cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight());
		cam.position.set(10f, 10f, 10f);
		cam.lookAt(0, 0, 0);
		cam.near = 0.1f;
		cam.far = 300f;
		cam.update();

		camController = new CameraInputController(cam);
		Gdx.input.setInputProcessor(camController);

		assetManager = new AssetManager();
		assetManager.load("data/threeD/player_mhx.g3db", Model.class);

		modelBatch = new ModelBatch();

		environment = new Environment();
		environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f,
				0.4f, 0.4f, 1f));
		environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f,
				-0.8f, -0.2f));

	}

	public void initPlayer() {

		Model playerModel = assetManager.get("data/threeD/player_mhx.g3db",
				Model.class);
		player = new ModelInstance(playerModel);
		player.transform.translate(0f, 0f, 0f);
		animationController = new AnimationController(player);
		playerLoadTime = TimeUtils.millis();
		playerLoadDuration = playerLoadTime - createTime;
		Gdx.app.log("", "Player loaded at: " + playerLoadTime);
		playerNotInited = false;
		Gdx.app.log("","No. of animations: "+player.animations.size);
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void render() {
		// If asset manager has finished loading assets but player has not been
		// initialized yet, then initialize player
		if (playerNotInited && assetManager.update()) {
			initPlayer();
		}

		camController.update();

		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight());
		Gdx.gl.glClearColor(1f, 1f, 1f, 1f);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

		modelBatch.begin(cam);
		if (player != null) {
			modelBatch.render(player, environment);
			Gdx.app.log("", "Rendering player...");
		}
		modelBatch.render(ci);
		modelBatch.end();
		
		//int matSize = player.materials.size;
		

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		modelBatch.dispose();
		assetManager.dispose();
	}

}
