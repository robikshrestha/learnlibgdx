package com.coderiders.learnlibgdx;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class InputDemo implements ApplicationListener,InputProcessor{

	private BitmapFont font;
	private SpriteBatch spriteBatch;
	private String text="";
	
	
	@Override
	public boolean keyDown(int keycode) {
		text="Keydown event: Keycode: "+keycode;
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		text="Keyup event: Keycode: "+keycode;
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		text="KeyTyped event: char: "+character;
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		text="touchDown event: screenX: "+screenX+" screenY: "+screenY+" pointer: "+pointer+" button: "+button;
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		text="touchUP: screenx: "+screenX+" screenY: "+screenY+" pointer: "+pointer+" button: "+button;
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		text="touchDragged: screenX: "+screenX+" screenY: "+screenY+" pointer: "+pointer;
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		text="mouseMoved: screenX: "+screenX+" screenY: "+screenY;
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		text="scrolled: "+amount;
		return false;
	}

	@Override
	public void create() {
		font = new BitmapFont();
		font.setScale(2f);
		font.setColor(Color.GREEN);
		spriteBatch = new SpriteBatch();
		Gdx.input.setInputProcessor(this);
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		spriteBatch.begin();
		font.draw(spriteBatch, text, 10, 80);
		spriteBatch.end();
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		spriteBatch.dispose();
		font.dispose();
	}

}
