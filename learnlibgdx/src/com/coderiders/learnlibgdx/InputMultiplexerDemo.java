package com.coderiders.learnlibgdx;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;

public class InputMultiplexerDemo implements ApplicationListener, InputProcessor, GestureListener{
	
	private List<String> messages = new ArrayList();
	private SpriteBatch spriteBatch;
	private BitmapFont font;
	
	private void addMessage(String s) {
		if (messages.size() > 25) {
			messages.remove(0);
		}
		messages.add(s);
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		addMessage("touchDown: x: " + x + " y: " + y + " ptr:" + pointer
				+ " btn: " + button);
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		addMessage("tap: x: "+x+" y: "+y+" cnt: "+count+" btn: "+button);
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		addMessage("longPres: x: "+x+" y: "+y);
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		addMessage("fling: vx: "+velocityX+" vy: "+velocityY+" btn: "+button);
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		addMessage("pan: x: "+x+" y: "+y+" deltaX: "+deltaX+" deltaY: "+deltaY);
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		addMessage("panStop: x: "+x+" y: "+y+" ptr: "+pointer+" btn: "+button);
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		addMessage("zoom: initialDistance: "+initialDistance+" distance: "+distance);
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
			Vector2 pointer1, Vector2 pointer2) {
		addMessage("pinch:");
		return false;
	}

	@Override
	public void create() {
		spriteBatch = new SpriteBatch();
		font = new BitmapFont();
		font.setColor(Color.GREEN);
		InputMultiplexer im = new InputMultiplexer();
		im.addProcessor(this);
		GestureDetector gd = new GestureDetector(this);
		im.addProcessor(gd);
		Gdx.input.setInputProcessor(im);
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(Gdx.gl10.GL_COLOR_BUFFER_BIT);
		spriteBatch.begin();
		for(int i=0;i<messages.size();i++){
			font.draw(spriteBatch, messages.get(i),20 , i*20+20);
		}
		spriteBatch.end();

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean keyDown(int keycode) {
		addMessage("keyDown: "+keycode);
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		addMessage("keyUp: "+keycode);
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		addMessage("keyTyped "+character);
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		addMessage("touchDown sx: "+screenX+" sy: "+screenY+" ptr: "+pointer+" btn: "+button);
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		addMessage("touchUp: sx: "+screenX+" sy: "+screenY+" ptr: "+pointer+" btn: "+button);
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		addMessage("touchDragged: sx: "+screenX+" sy: "+screenY+" ptr: "+pointer);
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		//addMessage("mouseMoved: sx: "+screenX+" sy: "+screenY);//too noisy!
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		addMessage("scrolled: "+amount);
		return false;
	}
}
