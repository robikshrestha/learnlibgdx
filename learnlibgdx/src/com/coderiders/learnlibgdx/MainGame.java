package com.coderiders.learnlibgdx;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class MainGame implements ApplicationListener {

	private SpriteBatch spriteBatch;
	private BitmapFont font;
	private Texture texture;
	private Sprite backgroundSprite, playerSprite,staticPlayerSprite;
	private TextureAtlas textureAtlas;
	private int currentFrame = 0;
	private int maxFrames = -1;
	private int w, h;

	@Override
	public void create() {
		createBatch();
		createFont();
		createPlayer();
		createBackground();
	}

	private void createPlayer() {
		textureAtlas = new TextureAtlas(
				Gdx.files.internal("data/images/skills/left_juggle.atlas"));
		playerSprite = new Sprite(textureAtlas.getRegions().first());
		staticPlayerSprite = new Sprite(textureAtlas.getRegions().first());
		maxFrames = textureAtlas.getRegions().size;
		Timer.schedule(new Task() {
			@Override
			public void run() {
				currentFrame++;
				if (currentFrame > maxFrames - 1)
					currentFrame = 1;

				playerSprite.setRegion(textureAtlas.getRegions().get(
						currentFrame));
				
			}
		}, 0, 1 / 10.0f);
	}

	private void createBatch() {
		spriteBatch = new SpriteBatch();
	}

	private void createFont() {
		font = new BitmapFont();
		font.setColor(Color.BLUE);
	}

	private void createBackground() {
		texture = new Texture(
				Gdx.files.internal("data/images/ui-packed/ui.png"));
		backgroundSprite = new Sprite(texture);
	}

	@Override
	public void resize(int width, int height) {
		w = width;
		h = height;
	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		spriteBatch.begin();
		backgroundSprite.draw(spriteBatch);
		playerSprite.draw(spriteBatch);
		//staticPlayerSprite.draw(spriteBatch);
		font.draw(spriteBatch, "Soccer Skillz", 20, h - 50);
		font.setScale(3f);
		spriteBatch.end();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		spriteBatch.dispose();
		texture.dispose();
		
	}

}
