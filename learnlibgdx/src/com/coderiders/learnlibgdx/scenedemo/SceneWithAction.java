package com.coderiders.learnlibgdx.scenedemo;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;

public class SceneWithAction implements ApplicationListener{

	private Stage stage;
	private Actor someActor;
	private MoveToAction moveToAction;
	@Override
	public void create() {
		stage = new Stage();
		moveToAction = new MoveToAction();
		moveToAction.setPosition(400f, 0);
		moveToAction.setDuration(10f);
		someActor = new SomeActor();
		someActor.addAction(moveToAction);
		stage.addActor(someActor);
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render() {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
