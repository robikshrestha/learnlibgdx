package com.coderiders.learnlibgdx.scenedemo;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;

public class SceneDemo implements ApplicationListener {

	private Stage stage;
	private Actor someActor;
	
	@Override
	public void create() {
		stage = new Stage();
		someActor = new SomeActor();
		someActor.setTouchable(Touchable.enabled);
		stage.addActor(someActor);
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render() {
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {

		((SomeActor)someActor).dispose();
		stage.dispose();
		
	}

}
