package com.coderiders.learnlibgdx.scenedemo;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class UIDemo implements ApplicationListener{

	private Skin skin;
	private Stage stage;
	
	@Override
	public void create() {
		stage = new Stage();
		skin = new Skin(Gdx.files.internal("data/skin/uiskin.json"));
		final TextButton btn = new TextButton("Click me", skin, "default");
		btn.setWidth(200f);
		btn.setHeight(60f);
		btn.setPosition(Gdx.graphics.getWidth()/3, Gdx.graphics.getHeight()/2);
		btn.addListener(new EventListener() {
			
			@Override
			public boolean handle(Event event) {
				btn.setText("I was sth!");
				return false;
			}
		});
		
		btn.addListener(new ClickListener(){

			@Override
			public void clicked(InputEvent event, float x, float y) {
				// TODO Auto-generated method stub
				super.clicked(event, x, y);
				btn.setText("I was clicked!");
			}
			
		});
		stage.addActor(btn);
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render() {
		stage.draw();
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();
		
	}

}
