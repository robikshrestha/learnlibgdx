package com.coderiders.learnlibgdx.scenedemo;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.Disposable;

public class SomeActor extends Actor implements Disposable{

	private TextureAtlas atlas;
	private Texture texture;
	private int x,y;
	private boolean started=false;
	
	public SomeActor(){
		
		atlas = new TextureAtlas(Gdx.files.internal("data/images/ui-packed/ui.atlas"));
		texture = atlas.getTextures().iterator().next();
		setBounds(getX(),getY(),texture.getWidth(),texture.getHeight());
		addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				System.out.println("touchdown called");
				started=true;
				return true;
			}
		});
	}
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		batch.draw(texture,this.getX(),this.getY());
	}
	@Override
	public void dispose() {
		texture.dispose();
		atlas.dispose();
	}
	@Override
	public void act(float delta) {
		super.act(delta);
		if(started){
			x+=5;
			y-=5;
		}
	}
	
	
	

}
