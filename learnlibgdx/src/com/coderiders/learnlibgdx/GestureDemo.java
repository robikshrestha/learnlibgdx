package com.coderiders.learnlibgdx;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;

public class GestureDemo implements ApplicationListener, GestureListener {

	SpriteBatch spriteBatch;
	BitmapFont font;
	List<String> messages = new ArrayList();

	private void addMessage(String s) {
		if (messages.size() > 5) {
			messages.remove(0);
		}
		messages.add(s);
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		addMessage("touchDown: x: " + x + " y: " + y + " ptr:" + pointer
				+ " btn: " + button);
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		addMessage("tap: x: "+x+" y: "+y+" cnt: "+count+" btn: "+button);
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		addMessage("longPres: x: "+x+" y: "+y);
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		addMessage("fling: vx: "+velocityX+" vy: "+velocityY+" btn: "+button);
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		addMessage("pan: x: "+x+" y: "+y+" deltaX: "+deltaX+" deltaY: "+deltaY);
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		addMessage("panStop: x: "+x+" y: "+y+" ptr: "+pointer+" btn: "+button);
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		addMessage("zoom: initialDistance: "+initialDistance+" distance: "+distance);
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
			Vector2 pointer1, Vector2 pointer2) {
		addMessage("pinch:");
		return false;
	}

	@Override
	public void create() {
		spriteBatch = new SpriteBatch();
		font = new BitmapFont();
		font.setColor(Color.GREEN);
		GestureDetector gd = new GestureDetector(this);
		Gdx.input.setInputProcessor(gd);
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(Gdx.gl10.GL_COLOR_BUFFER_BIT);
		spriteBatch.begin();
		for(int i=0;i<messages.size();i++){
			font.draw(spriteBatch, messages.get(i),20 , i*20+20);
		}
		spriteBatch.end();

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
