package com.coderiders.learnlibgdx;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.coderiders.learnlibgdx.scenedemo.SceneDemo;
import com.coderiders.learnlibgdx.scenedemo.SceneWithAction;
import com.coderiders.learnlibgdx.scenedemo.UIDemo;
import com.coderiders.learnlibgdx.threeD.ThreeD;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Learn Libgdx";
		cfg.useGL20 = true;
		cfg.width = 800;
		cfg.height = 600;
		
		//new LwjglApplication(new MainGame(), cfg);
		//new LwjglApplication(new InputDemo(), cfg);
		//new LwjglApplication(new GestureDemo(), cfg);
		//new LwjglApplication(new InputMultiplexerDemo(), cfg);
		//new LwjglApplication(new SoundDemo(), cfg);
		//new LwjglApplication(new SceneDemo(), cfg);
		//new LwjglApplication(new SceneWithAction(), cfg);
		//new LwjglApplication(new UIDemo(), cfg);
		new LwjglApplication(new ThreeD(), cfg);
	}
}
